package au.edu.sydney.domain;

import junit.framework.TestCase;

public class UserTest extends TestCase {
	private User user;

	protected void setUp() throws Exception {
		user = new User();
	}

	public void testSetAndGetUsername() {
		String testUsername = "aUsername";
		assertNull(user.getUsername());
		user.setUsername(testUsername);
		assertEquals(testUsername, user.getUsername());
	}

	public void testSetAndGetPassword() {
		String testPassword = "aPassword";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		;
		assertEquals(testPassword, user.getPassword());
	}

	public void testSetAndGetUserType() {
		int testUserType = 1;
		assertEquals(0, 0, 0);
		user.setUserType(1);
		assertEquals(testUserType, user.getUserType(), 0);
	}

	public void testSetAndGetFirstname() {
		String testFirstname = "aFirstname";
		assertNull(user.getFirstname());
		user.setFirstname(testFirstname);
		assertEquals(testFirstname, user.getFirstname());
	}

	public void testSetAndGetLastname() {
		String testLastname = "aLastname";
		assertNull(user.getLastname());
		user.setLastname(testLastname);
		assertEquals(testLastname, user.getLastname());
	}

	public void testSetAndGetDob() {
		String testDob = "aDob";
		assertNull(user.getDob());
		user.setDob(testDob);
		assertEquals(testDob, user.getDob());
	}

	public void testSetAndGetPhone() {
		String testPhone = "aPhone";
		assertNull(user.getPhone());
		user.setPhone(testPhone);
		assertEquals(testPhone, user.getPhone());
	}

	public void testSetAndGetEmail() {
		String testEmail = "aEmail";
		assertNull(user.getEmail());
		user.setEmail(testEmail);
		assertEquals(testEmail, user.getEmail());
	}

	public void testSetAndGetPassport() {
		String testPassport = "aPassport";
		assertNull(user.getPassport());
		user.setPassport(testPassport);
		assertEquals(testPassport, user.getPassport());
	}
}
