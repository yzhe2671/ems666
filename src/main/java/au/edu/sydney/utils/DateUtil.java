package au.edu.sydney.utils;

public class DateUtil {
	public static String toYMD(String date) {
		return date.substring(0, date.indexOf(" "));
	}

	public static String toYMDHMS(String date) {
		return date.substring(0, date.indexOf("."));
	}
}
