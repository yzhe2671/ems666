package au.edu.sydney.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.sydney.domain.User;
import au.edu.sydney.service.UserManager;
import au.edu.sydney.utils.DateUtil;

@Controller
@RequestMapping(value = "/account")
public class AccountController {

	@Autowired
	private UserManager userManager;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String viewRegistration(Map<String, Object> model) {
		User userForm = new User();
		model.put("userForm", userForm);

		return "user_registration";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processRegistration(@ModelAttribute("userForm") User user, HttpServletRequest request) {

		if (userManager.registerUser(user)) {
			User current_user = userManager.getUser(user.getUsername());
			current_user.setDob(DateUtil.toYMD(current_user.getDob()));
			request.getSession().setAttribute("user", current_user);
			return "user_registration_success";
		}

		return "user_registration_failure";

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String viewLogin() {
		return "user_login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String processLogin(@RequestParam("username") String username, @RequestParam("password") String password,
			HttpServletRequest request) {

		if (userManager.loginUser(username, password)) {
			User current_user = userManager.getUser(username);
			current_user.setDob(DateUtil.toYMD(current_user.getDob()));
			request.getSession().setAttribute("user", current_user);
			return "user_login_success";
		}

		return "user_login_failure";

	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewMyAccount(Map<String, Object> model, HttpServletRequest request) {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			return "account_home";
		}
		return "user_login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logOut(HttpServletRequest request) {
		request.getSession().removeAttribute("user");
		return "user_login";
	}
}
