package au.edu.sydney.web;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.EventImage;
import au.edu.sydney.service.MediaManager;

@Controller
@RequestMapping(value = "/media")
public class MediaController {
	@Autowired
	private MediaManager mediaManager;

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String viewUpload(HttpServletRequest request) {
		request.setAttribute("eventId", request.getParameter("eventId"));
		return "media_upload";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String processUpload(@RequestParam(value = "file", required = false) MultipartFile[] files,
			HttpServletRequest request) throws Exception {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			if ((Boolean) request.getAttribute("authorized")) {
				int eventId = 0;
				try {
					eventId = Integer.parseInt(request.getParameter("eventId"));
				} catch (Exception e) {
					e.printStackTrace();
				}

				String pathRoot = request.getSession().getServletContext().getRealPath("");
				String path = "";

				EventImage image = new EventImage();
				EventAudio audio = new EventAudio();

				if (eventId != 0) {
					for (MultipartFile file : files) {
						if (!file.isEmpty()) {
							String uuid = UUID.randomUUID().toString().replaceAll("-", "");
							String contentType = file.getContentType();
							System.out.println(contentType);
							String fileName = contentType.substring(contentType.indexOf("/") + 1);

							if (fileName.equals("mpeg")) {
								fileName = "mp3";
							}

							if (contentType.contains("image")) {
								path = "/static_resources/images/" + uuid + "." + fileName;
								image.setEventId(eventId);
								image.setImageUrl(path);
							}
							if (contentType.contains("audio")) {
								path = "/static_resources/audios/" + uuid + "." + fileName;
								audio.setEventId(eventId);
								audio.setAudioUrl(path);
							}
							file.transferTo(new File(pathRoot + path));
						}
					}
				}

				List<EventImage> eventImages = mediaManager.getEventImages(eventId);
				List<EventAudio> eventAudios = mediaManager.getEventAudios(eventId);
				request.setAttribute("eventId", request.getParameter("eventId"));

				if (eventImages.isEmpty() && eventAudios.isEmpty()) {
					boolean imageSaved = mediaManager.addEventImage(image);
					boolean audioSaved = mediaManager.addEventAudio(audio);

					if (imageSaved || audioSaved) {
						return "media_upload_success";
					}
				} else {
					if (!eventImages.isEmpty()) {
						for (Iterator<EventImage> iterator = eventImages.iterator(); iterator.hasNext();) {
							EventImage eventImage = iterator.next();
							eventImage.setImageUrl(image.getImageUrl());
							mediaManager.updateEventImage(eventImage);
						}
					}
					if (!eventAudios.isEmpty()) {
						for (Iterator<EventAudio> iterator = eventAudios.iterator(); iterator.hasNext();) {
							EventAudio eventAudio = iterator.next();
							eventAudio.setAudioUrl(audio.getAudioUrl());
							mediaManager.updateEventAudio(eventAudio);
						}
					}
					return "media_upload_success";
				}
				return "media_upload_failure";
			}
		} else {
			return "user_login";
		}
		return null;
	}

}
