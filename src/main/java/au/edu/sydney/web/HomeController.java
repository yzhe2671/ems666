package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.MediaEventPair;
import au.edu.sydney.service.CategoryManager;
import au.edu.sydney.service.EventManager;
import au.edu.sydney.service.MediaManager;

/**
 * Handles requests for the application home page.
 */
@Controller
// make the operations are transactional
@Transactional
public class HomeController {
	@Autowired
	private CategoryManager categoryManager;

	@Autowired
	private EventManager eventManager;

	@Autowired
	private MediaManager mediaManager;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpServletRequest request) {
		List<Event> events = eventManager.retrieveLatestNEvents(20);

		List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
		for (Iterator<Event> iterator = events.iterator(); iterator.hasNext();) {
			Event event = iterator.next();
			mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()),
					mediaManager.getEventAudios(event.getEventId())));
		}
		request.getSession().setAttribute("mediaEventPairs", mediaEventPairs);
		request.getSession().setAttribute("categories", categoryManager.getCategories());
		return "home";
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact() {
		return "contact";
	}
	@RequestMapping(value = "/map", method = RequestMethod.GET)
	public String GoogleMap() {
		return "GoogleMap";
	}
}
