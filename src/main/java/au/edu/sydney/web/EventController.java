package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.MediaEventPair;
import au.edu.sydney.service.CategoryManager;
import au.edu.sydney.service.EventManager;
import au.edu.sydney.service.MediaManager;
import au.edu.sydney.utils.DateUtil;

@Controller
@RequestMapping(value = "/event")
public class EventController {

	@Autowired
	private EventManager eventManager;

	@Autowired
	private CategoryManager categoryManager;

	@Autowired
	private MediaManager mediaManager;

	@RequestMapping(value = "/post", method = RequestMethod.GET)
	public String viewPost(Map<String, Object> model, HttpServletRequest request) {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			Event eventForm = new Event();
			model.put("eventForm", eventForm);

			model.put("categoryNames", categoryManager.getCategoryNames());

			return "event_post";
		}
		return "user_login";
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String processPost(@ModelAttribute("eventForm") Event event, HttpServletRequest request) {

		if (eventManager.postEvent(event)) {
			request.setAttribute("eventId",
					eventManager.retrieveEvent(event.getOrganizer(), event.getEventName()).getEventId());
			return "event_post_edit_success";
		}

		return "event_post_edit_failure";

	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewEvent(HttpServletRequest request) {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("eventId"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (eventId != 0) {
			Event event = eventManager.retrieveEvent(eventId);

			event.setStartDate(DateUtil.toYMDHMS(event.getStartDate()));
			event.setEndDate(DateUtil.toYMDHMS(event.getEndDate()));

			MediaEventPair mediaEventPair = new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()),
					mediaManager.getEventAudios(event.getEventId()));
			request.setAttribute("mediaEventPair", mediaEventPair);

			return "event_home";
		}

		return null;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String viewEventsByCategory(HttpServletRequest request) {
		String category = request.getParameter("category");

		if (category != null) {
			List<Event> events = eventManager.retrieveEventsByCategory(category);
			List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
			for (Iterator<Event> iterator = events.iterator(); iterator.hasNext();) {
				Event event = iterator.next();
				mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()),
						mediaManager.getEventAudios(event.getEventId())));
			}
			request.setAttribute("category", category);
			request.setAttribute("mediaEventPairs", mediaEventPairs);
			return "events_by_category";
		}

		return null;
	}

	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	public String viewMyPosts(Map<String, Object> model, HttpServletRequest request) {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			if ((Boolean) request.getAttribute("authorized")) {
				String username = request.getParameter("username");
				List<Event> events = eventManager.retrieveEventsByOrganizer(username);
				if (!events.isEmpty()) {
					request.setAttribute("events", events);
				}
				return "my_posts";
			}
		} else {
			return "user_login";
		}
		return null;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editPost(Map<String, Object> model, HttpServletRequest request) {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			if ((Boolean) request.getAttribute("authorized")) {
				int eventId = 0;
				try {
					eventId = Integer.parseInt(request.getParameter("eventId"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (eventId != 0) {
					Event oldEvent = eventManager.retrieveEvent(eventId);
					Event newEvent = new Event();

					model.put("oldEvent", oldEvent);
					model.put("newEvent", newEvent);

					model.put("categoryNames", categoryManager.getCategoryNames());

					return "event_edit";
				}
			}
		} else {
			return "user_login";
		}
		return null;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String processEdit(@ModelAttribute("newEvent") Event event, HttpServletRequest request) {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("eventId"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (eventId != 0) {
			event.setEventId(eventId);
			eventManager.updateEvent(event);
			request.setAttribute("update", true);
			request.setAttribute("eventId", eventId);
			return "event_post_edit_success";
		}
		return "event_post_edit_failure";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deletePost(Map<String, Object> model, HttpServletRequest request) {
		if ((Boolean) request.getAttribute("hasLoggedIn")) {
			if ((Boolean) request.getAttribute("authorized")) {
				int eventId = 0;
				try {
					eventId = Integer.parseInt(request.getParameter("eventId"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (eventId != 0) {
					eventManager.deleteEvent(eventManager.retrieveEvent(eventId));
					return "event_delete_success";
				}
			}
		} else {
			return "user_login";
		}
		return null;
	}
}
