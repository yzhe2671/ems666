package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.MediaEventPair;
import au.edu.sydney.domain.User;
import au.edu.sydney.service.CategoryManager;
import au.edu.sydney.service.EventManager;
import au.edu.sydney.service.MediaManager;
import au.edu.sydney.service.UserManager;

@Controller
public class SearchController {
	
	@Autowired
	private CategoryManager categoryManager;
	
	@Autowired
	private EventManager eventManager;

	@Autowired
	private MediaManager mediaManager;
	
	
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
		public String search(HttpServletRequest request) {
		String Keyword=request.getParameter("keyword");
			List<Event> events = eventManager.retrieveEventByName(Keyword);
			if(!events.isEmpty()){
			List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
			for (Iterator<Event> iterator = events.iterator(); iterator.hasNext();) {
				Event event = iterator.next();
				mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()),
						mediaManager.getEventAudios(event.getEventId())));
			}
			request.getSession().setAttribute("mediaEventPairs", mediaEventPairs);
			request.getSession().setAttribute("categories", categoryManager.getCategories());
		return "search";
		    }
			else{
				return "no_search_result";
			}
		}
	
	
		@RequestMapping(value = "/searchindetail", method = RequestMethod.GET)
			public String searchindetail() {
			return "search_in_detail";
	}
		
	  @RequestMapping(value = "/searchresult", method = RequestMethod.GET)
		public String searchbydetail(HttpServletRequest request) {
			String eventname=request.getParameter("EventName");
			String category=request.getParameter("EventCategory");
			String organizer=request.getParameter("EventOrganizer");
			String location=request.getParameter("EventLocation");
				List<Event> events = eventManager.retrieveEventByDetail(eventname, category, organizer, location);
				if(!events.isEmpty()){
				List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
				for (Iterator<Event> iterator = events.iterator(); iterator.hasNext();) {
					Event event = iterator.next();
					mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()),
							mediaManager.getEventAudios(event.getEventId())));
				}
				request.getSession().setAttribute("mediaEventPairs", mediaEventPairs);
				request.getSession().setAttribute("categories", categoryManager.getCategories());
			return "search";
			    }
				else{
					return "no_search_result";
				}
			} 
	  
	  @RequestMapping(value = "/showlocationinmap", method = RequestMethod.GET)
		public String showlocationinmap(HttpServletRequest request) {
		  String latitude = request.getParameter("latitude");
		  String longitude = request.getParameter("longitude");
			request.getSession().setAttribute("latitude", latitude);
			request.getSession().setAttribute("longitude", longitude);

		  return "Map_show_location";
		  
	  }


	
	  
	
	

}
