package au.edu.sydney.aop;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import au.edu.sydney.domain.Booking;
import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.User;
import au.edu.sydney.service.BookingManager;
import au.edu.sydney.service.EventManager;

//@Aspect
//@Component
public class AuthenticationInterceptor {
	@Autowired
	private BookingManager bookingManager;

	@Autowired
	private EventManager eventManager;

	// @Around("execution(public String
	// au.edu.sydney.web.BookingController.cancelBooking(javax.servlet.http.HttpServletRequest))")
	public void authenticate(Object model, HttpServletRequest request) {
		System.out.println("authenticate");
		int eventId = 0;
		int bookingId = 0;
		String username = request.getParameter("username");
		User user = (User) request.getSession().getAttribute("user");

		if (user != null) {
			request.setAttribute("hasLoggedIn", true);
			try {
				eventId = Integer.parseInt(request.getParameter("eventId"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				bookingId = Integer.parseInt(request.getParameter("bookingId"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (eventId != 0) {
				Event event = eventManager.retrieveEvent(eventId);
				if (event != null) {
					if (user.getUsername().equals(event.getOrganizer())) {
						request.setAttribute("authorized", true);
						return;
					}
				}
			}
			if (bookingId != 0) {
				Booking booking = bookingManager.retrieveBooking(bookingId);
				if (booking != null) {
					if (user.getUsername().equals(booking.getUsername())) {
						request.setAttribute("authorized", true);
						return;
					}
				}
			}
			if (username != null) {
				if (user.getUsername().equals(username)) {
					request.setAttribute("authorized", true);
					request.setAttribute("username", username);
					return;
				}
			}
		} else {
			request.setAttribute("hasLoggedIn", false);
		}
		request.setAttribute("authorized", false);
	}
}
