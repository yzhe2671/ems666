package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.CategoryDao;
import au.edu.sydney.domain.Category;

@Service(value = "categoryManager")
@Transactional
public class CategoryManager {

	@Autowired
	private CategoryDao categoryDao;

	// business logic of registering a Person into the database
	public boolean registerCategory(Category category) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!isExistingCategory(category)) {
			categoryDao.saveCategory(category);
			return true;
		}
		System.out.println("categoryName already exits.");
		return false;
	}
	
	public boolean isExistingCategory(Category category) {
		return categoryDao.getCategory(category.getCategoryName()) != null;
	}

	public List<String> getCategoryNames() {
		return categoryDao.getCategoryNames();
	}

	public List<Category> getCategories() {
		return categoryDao.getCategories();
	}
}
