package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.EventDao;
import au.edu.sydney.domain.Event;

@Service(value = "eventManager")
@Transactional
public class EventManager {

	@Autowired
	private EventDao eventDao;

	public boolean postEvent(Event event) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!isExistingEvent(event)) {
			eventDao.saveEvent(event);
			return true;
		}
		System.out.println("eventName already exits.");
		return false;
	}
	
	public boolean isExistingEvent(Event event) {
		return eventDao.getEvent(event.getOrganizer(), event.getEventName()) != null;
	}
	
	

	public Event retrieveEvent(String organizer, String eventName) {
		return eventDao.getEvent(organizer, eventName);
	}

	public Event retrieveEvent(int eventId) {
		return eventDao.getEvent(eventId);
	}

	public List<Event> retrieveEventsByOrganizer(String organizer) {
		return eventDao.getEventsByOrganizer(organizer);
	}

	public List<Event> retrieveEventsByCategory(String category) {
		return eventDao.getEventsByCategory(category);
	}

	public List<Event> retrieveLatestNEvents(int n) {
		return eventDao.getLatestNEvents(n);
	}
	
	public List<Event> retrieveEventByName(String eventName){
		return eventDao.getEventsByName(eventName);
	}
	
	public List<Event> retrieveEventsByLocation(String location){
		return eventDao.getEventByLocation(location);
	}
	
	public List<Event> retrieveEventByDetail(String name,String category, String organizer, String location){
		return eventDao.getEventByDetail(name,category,organizer,location);
	} 

	public void deleteEvent(Event event) {
		eventDao.deleteEvent(event);
	}

	public void updateEvent(Event event) {
		eventDao.updateEvent(event);
	}
	
}
