package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.MediaDao;
import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.EventImage;

@Service(value = "mediaManager")
@Transactional
public class MediaManager {

	@Autowired
	private MediaDao mediaDao;

	// business logic of registering a Person into the database
	public boolean addEventImage(EventImage eventImage) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (isValidImage(eventImage)) {
			mediaDao.saveImage(eventImage);
			return true;
		}
		return false;
	}

	public boolean addEventAudio(EventAudio eventAudio) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (isValidAudio(eventAudio)) {
			mediaDao.saveAudio(eventAudio);
			return true;
		}
		return false;
	}

	private boolean isValidImage(EventImage eventImage) {
		if (eventImage.getEventId() != 0) {
			if (eventImage.getImageUrl() != null) {
				return true;
			}
		}
		return false;
	}

	private boolean isValidAudio(EventAudio eventAudio) {
		if (eventAudio.getEventId() != 0) {
			if (eventAudio.getAudioUrl() != null) {
				return true;
			}
		}
		return false;
	}

	public List<EventImage> getEventImages(int eventId) {
		return mediaDao.getImages(eventId);
	}

	public List<EventAudio> getEventAudios(int eventId) {
		return mediaDao.getAudios(eventId);
	}

	public void updateEventImage(EventImage eventImage) {
		mediaDao.updateImage(eventImage);
	}

	public void updateEventAudio(EventAudio eventAudio) {
		mediaDao.updateAudio(eventAudio);
	}

}
