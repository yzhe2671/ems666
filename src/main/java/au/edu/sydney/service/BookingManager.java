package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.BookingDao;
import au.edu.sydney.domain.Booking;

@Service(value = "bookingManager")
@Transactional
public class BookingManager {

	@Autowired
	private BookingDao bookingDao;

	// business logic of registering a Person into the database
	public boolean registerBooking(Booking booking) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!isExistingBooking(booking)) {
			bookingDao.saveBooking(booking);
			return true;
		}
		return false;
	}
	
	public boolean isExistingBooking(Booking booking) {
		return bookingDao.getBooking(booking.getUsername(), booking.getEventId()) != null;
	}

	public Booking retrieveBooking(String username, int eventId) {
		return bookingDao.getBooking(username, eventId);
	}

	public List<Booking> retrieveBookings(String username) {
		return bookingDao.getBookings(username);
	}

	public void cancelBooking(int bookingId) {
		bookingDao.deleteBooking(bookingDao.getBooking(bookingId));
	}

	public Booking retrieveBooking(int bookingId) {
		return bookingDao.getBooking(bookingId);
	}
}
