package au.edu.sydney.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.UserDao;
import au.edu.sydney.domain.User;

@Service(value = "userManager")
@Transactional
public class UserManager {

	@Autowired
	private UserDao userDao;

	// business logic of registering a Person into the database
	public boolean registerUser(User user) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!isExistingUser(user)) {
			userDao.saveUser(user);
			return true;
		}
		System.out.println("username already exits.");
		return false;
	}

	public boolean loginUser(String username, String password) {
		User user = userDao.getUser(username);
		if (user != null) {
			return user.getPassword().equals(password);
		}
		return false;
	}

	public User getUser(String username) {
		return userDao.getUser(username);
	}

	public boolean isExistingUser(User user) {
		return userDao.getUser(user.getUsername()) != null;
	}
}
