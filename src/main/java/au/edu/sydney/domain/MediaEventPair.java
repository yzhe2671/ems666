package au.edu.sydney.domain;

import java.util.List;

public class MediaEventPair {
	private Event event;
	private List<EventImage> images;
	private List<EventAudio> audios;

	public MediaEventPair(Event event, List<EventImage> images, List<EventAudio> audios) {
		super();
		this.event = event;
		this.images = images;
		this.audios = audios;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public List<EventImage> getImages() {
		return images;
	}

	public void setImages(List<EventImage> images) {
		this.images = images;
	}

	public List<EventAudio> getAudios() {
		return audios;
	}

	public void setAudios(List<EventAudio> audios) {
		this.audios = audios;
	}

}
