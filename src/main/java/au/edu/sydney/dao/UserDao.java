package au.edu.sydney.dao;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.User;

@Repository(value = "userDao")
public class UserDao {

	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().save(user);
	}

	public User getUser(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Expression.like("username", username));
		return (User) criteria.uniqueResult();

	}
}
