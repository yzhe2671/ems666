package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.EventImage;

@Repository(value = "mediaDao")
public class MediaDao {

	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveImage(EventImage image) {
		sessionFactory.getCurrentSession().save(image);
	}

	public void saveAudio(EventAudio audio) {
		sessionFactory.getCurrentSession().save(audio);
	}

	public EventImage getImage(int eventId, String imageUrl) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EventImage.class);
		criteria.add(Expression.like("eventId", eventId)).add(Expression.like("imageUrl", imageUrl));
		return (EventImage) criteria.uniqueResult();
	}

	public EventAudio getAudio(int eventId, String audioUrl) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EventAudio.class);
		criteria.add(Expression.like("eventId", eventId)).add(Expression.like("audioUrl", audioUrl));
		return (EventAudio) criteria.uniqueResult();
	}

	public List<EventImage> getImages(int eventId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EventImage.class);
		criteria.add(Expression.like("eventId", eventId));
		return (List<EventImage>) criteria.list();
	}

	public List<EventAudio> getAudios(int eventId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(EventAudio.class);
		criteria.add(Expression.like("eventId", eventId));
		return (List<EventAudio>) criteria.list();
	}

	public void updateImage(EventImage image) {
		sessionFactory.getCurrentSession().update((Object) image);
	}

	public void updateAudio(EventAudio audio) {
		sessionFactory.getCurrentSession().update((Object) audio);
	}
}
