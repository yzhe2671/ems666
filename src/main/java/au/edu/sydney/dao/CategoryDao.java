package au.edu.sydney.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Category;

@Repository(value = "categoryDao")
public class CategoryDao {

	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveCategory(Category category) {
		sessionFactory.getCurrentSession().save(category);
	}

	public Category getCategory(String categoryName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Category.class);
		criteria.add(Expression.like("categoryName", categoryName));
		return (Category) criteria.uniqueResult();

	}

	public List<String> getCategoryNames() {
		List<Category> list = (List<Category>) sessionFactory.getCurrentSession().createCriteria(Category.class).list();
		List<String> categoryNames = new ArrayList<String>();
		for (Iterator<Category> iterator = list.iterator(); iterator.hasNext();) {
			categoryNames.add(iterator.next().getCategoryName());
		}
		return categoryNames;
	}

	public List<Category> getCategories() {
		return (List<Category>) sessionFactory.getCurrentSession().createCriteria(Category.class)
				.addOrder(Order.asc("categoryName")).list();
	}
}
