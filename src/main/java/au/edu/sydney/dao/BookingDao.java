package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Booking;

@Repository(value = "bookingDao")
public class BookingDao {

	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveBooking(Booking booking) {
		sessionFactory.getCurrentSession().save(booking);
	}

	public Booking getBooking(String username, int eventId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Booking.class);
		criteria.add(Expression.like("username", username)).add(Expression.like("eventId", eventId));
		return (Booking) criteria.uniqueResult();

	}

	public Booking getBooking(int bookingId) {
		return (Booking) sessionFactory.getCurrentSession().get(Booking.class, bookingId);
	}

	public List<Booking> getBookings(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Booking.class);
		criteria.add(Expression.like("username", username)).addOrder(Order.desc("bookingTime"));
		return (List<Booking>) criteria.list();

	}

	public void deleteBooking(Booking booking) {
		sessionFactory.getCurrentSession().delete((Object) booking);
	}

}
