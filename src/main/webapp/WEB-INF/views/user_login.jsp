<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Login</title>

<!-- Bootstrap core CSS -->
<link href="./static_resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./static_resources/css/jumbotron.css" rel="stylesheet">

</head>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="./">Event Bridge</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarsExampleDefault"
			aria-controls="navbarsExampleDefault" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<c:choose>
					<c:when test="${sessionScope.user != null}">
						<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
						<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
						<li class="nav-item dropdown">
				            <a class="nav-link dropdown-toggle" href="http://example.com/" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${sessionScope.user.username}</a>
				            <div class="dropdown-menu" aria-labelledby="dropdown01">
				              <a class="dropdown-item" href="account/home">My Account Home</a>
				              <a class="dropdown-item" href="./booking/home?username=${user.username}">My Bookings</a>
				              <a class="dropdown-item" href="./event/myPosts?username=${user.username}">My Posts</a>
				              <a class="dropdown-item" href="./account/logout">Log out</a>
				            </div>
         				 </li>
					</c:when>
					<c:otherwise>
							<!-- <li class="nav-item active"> -->
							<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="./account/register">Sign Up</a></li>
							<li class="nav-item"><a class="nav-link" href="./account/login">Log in</a></li>
							<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="./search">
				<input class="form-control mr-sm-2" type="text" placeholder="Search"
					aria-label="Search" name="keyword"> 
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
						<a class="nav-link" href="./searchindetail">Search in detail</a>
			
		</div>
	</nav>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container" id="loginContainer">
			<!-- Example row of columns -->
			<form action="account/login" method="post">
				<table align="center">
					<tr>
						<td colspan="2" align="center"><h2><fmt:message key="user_login.heading"/></h2></td>
					</tr>
					<tr>
						<td>Username:</td>
						<td><input type="text" name="username" required/></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" required/></td>
					</tr>
					<tr><td></td></tr>
					<tr>
						<td></td>
						<td align="center"><button type="submit" >Log in </button></td>
					</tr>
						<td></td>
						<td><a href="./reset_password">Forget your password?</a></td>
					<tr>
					</tr>
						<td></td>
						<td><a href="./account/register">Don't have an account?</a></td>
					<tr>
				</table>
			</form>
		</div>
	</div>

	<div class="container">
		<!-- Example row of columns -->

		<hr>

		<footer>
			<p>� Company 2017</p>
		</footer>
	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./static_resources/js/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="./static_resources/js/popper.min.js"></script>
	<script src="./static_resources/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./static_resources/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>