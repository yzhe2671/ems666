<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Register</title>

<!-- Bootstrap core CSS -->
<link href="./static_resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./static_resources/css/jumbotron.css" rel="stylesheet">
<style>
      #map {
        height: 100%;
        width: 100%;
       }
    </style>
    <style>
     .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        float:left;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
      
     #location_reminder {
          margin-right: 12px; 
          font-size: 16px;
          margin-left: 350px;
          }
    </style>

 
</style> 
</head>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="./">Event Bridge</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarsExampleDefault"
			aria-controls="navbarsExampleDefault" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	
		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<c:choose>
					<c:when test="${sessionScope.user != null}">
						<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
						<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
						<li class="nav-item dropdown">
				            <a class="nav-link dropdown-toggle" href="http://example.com/" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${sessionScope.user.username}</a>
				            <div class="dropdown-menu" aria-labelledby="dropdown01">
				              <a class="dropdown-item" href="account/home">My Account Home</a>
				              <a class="dropdown-item" href="./booking/home?username=${user.username}">My Bookings</a>
				              <a class="dropdown-item" href="./event/myPosts?username=${user.username}">My Posts</a>
				              <a class="dropdown-item" href="./account/logout">Log out</a>
				            </div>
         				 </li>
					</c:when>
					<c:otherwise>
							<!-- <li class="nav-item active"> -->
							<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="./account/register">Sign Up</a></li>
							<li class="nav-item"><a class="nav-link" href="./account/login">Log in</a></li>
							<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="./search">
				<input class="form-control mr-sm-2" type="text" placeholder="Search"
					aria-label="Search" name="keyword"> 
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
						<a class="nav-link" href="./searchindetail">Search in detail</a>
			
		</div>
	</nav>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container" >
			<!-- Example row of columns -->
			<form:form method="post" commandName="eventForm" action="event/post" id="form_input">
				<table align="center">
					<tr>
						<td colspan="2" align="center"><h2><fmt:message key="event_post.heading"/></h2></td>
					</tr>
					<tr>
						<td>Event name:</td>
						<td><form:input path="eventName" required="true"/></td>
						<%-- <td width="34%"><form:errors path="eventName" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td >Event organizer:</td>
						<td ><form:input path="organizer" required="true" value="${user.username}"/></td>
						<td width="34%"><form:errors path="organizer" cssClass="error"/></td>
					</tr>
					 <tr>
						<td>City</td>
						<td/><form:input path="location" required="true"/></td>
						<%-- <td width="34%"><form:errors path="location" cssClass="error"/></td> --%>
					</tr> 
					<tr>
						<td>Start Date (YYYY-MM-DD hh:mm):</td>
						<td><form:input path="startDate" required="true"/></td>
						<%-- <td width="34%"><form:errors path="startDate" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td>End Date (YYYY-MM-DD hh:mm):</td>
						<td><form:input path="endDate" required="true"/></td>
						<%-- <td width="34%"><form:errors path="endDate" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td>Capacity:</td>
						<td><form:input path="capacity" required="true"/></td>
						<%-- <td width="34%"><form:errors path="capacity" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td>Fees ($AUD):</td>
						<td><form:input path="fees" value="0.00" required="true"/></td>
						<%-- <td width="34%"><form:errors path="fees" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td>Category:</td>
						<td><form:select path="category" items="${categoryNames}" required="true"/></td>
						<%-- <td width="34%"><form:errors path="category" cssClass="error"/></td> --%>
					</tr>
					<tr>
						<td>Description:</td>
						<td><form:textarea path="description"/></td>
						<%-- <td width="34%"><form:errors path="description" cssClass="error"/></td> --%>
					</tr>
					<tr>
					<td></td>
					</tr>
					<tr>
					  
						
						<td>
							<button type="submit" >Submit</button>
							<button type="reset" >clear </button>
						</td>
					</tr>
				</table>
				<form:input type="hidden" path="latitude" id="lat"/>
				  	   <form:input type="hidden" path="longitude" id="lng"/>
			</form:form>
			<!-- here is the google map -->
			<div id="form_map">
	     <input id="pac-input" class="controls" type="text" placeholder="Search location">
	     <div id="location_reminder">Please put the marker in your event place, you can both drag the marker and search for location.</div>
    <div id="map"></div>
    <script>
    function initMap() {
        var sydney = {lat: -33.865143, lng: 151.209900};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: sydney
        });
        var marker = new google.maps.Marker({
          position: sydney,
          map: map,
          draggable:true
           });
        //....................search bar
        var searchBox =new google.maps.places.SearchBox(document.getElementById('pac-input'));
        google.maps.event.addListener(searchBox, 'places_changed',function(){
        	var places = searchBox.getPlaces();
        	var bounds = new google.maps.LatLngBounds();
        	var i,places;
        	for(i=0;place=places[i];i++){
        		//console.log(place.geometry.location);
        		
        		bounds.extend(place.geometry.location);
        		marker.setPosition(place.geometry.location);
        		
        		document.getElementById('lat').value = marker.getPosition().lat();
           		document.getElementById('lng').value = marker.getPosition().lng();
        		
        	}
        	map.fitBounds(bounds);
        	map.setZoom(15);
        	      		
        });
   function handleEvent(event){
        document.getElementById('lat').value = marker.getPosition().lat();
   		document.getElementById('lng').value = marker.getPosition().lng();
   		}
   
    document.getElementById('lat').value = marker.getPosition().lat();
	document.getElementById('lng').value = marker.getPosition().lng();
   
   marker.addListener('drag', handleEvent);
   marker.addListener('dragend', handleEvent);
        //.............................
        
       /* map.addListener('center_changed', function() {
            // 3 seconds after the center of the map has changed, pan back to the
            // marker.
            window.setTimeout(function() {
              map.panTo(marker.getPosition());
            }, 3000);
          }); */
        
       /* marker.addListener('click', function() {
            map.setZoom(8);
            map.setCenter(marker.getPosition());
            marker.set
          });*/
          
        /*google.maps.event.addListener(map,'click', function(event){
        	marker.setPosition(event.latLng);
        });
          */
    	
    }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf8ZNzhLvaxavUv2Ssy8ux4m7_hPC1wpE&callback=initMap&libraries=places">
    </script>
	     
	     </div>
		</div>
		
	     
		
	</div>

	<div class="container">
		<!-- Example row of columns -->

		<hr>

		<footer>
			<p>� Company 2017</p>
		</footer>
	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./static_resources/js/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="./static_resources/js/popper.min.js"></script>
	<script src="./static_resources/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./static_resources/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>