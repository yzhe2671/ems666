<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Event Home</title>

<!-- Bootstrap core CSS -->
<link href="./static_resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./static_resources/css/jumbotron.css" rel="stylesheet">

</head>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="./">Event Bridge</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarsExampleDefault"
			aria-controls="navbarsExampleDefault" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<c:choose>
					<c:when test="${sessionScope.user != null}">
						<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
						<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
						<li class="nav-item dropdown">
				            <a class="nav-link dropdown-toggle" href="http://example.com/" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${sessionScope.user.username}</a>
				            <div class="dropdown-menu" aria-labelledby="dropdown01">
				              <a class="dropdown-item" href="account/home">My Account Home</a>
				              <a class="dropdown-item" href="./booking/home?username=${user.username}">My Bookings</a>
				              <a class="dropdown-item" href="./event/myPosts?username=${user.username}">My Posts</a>
				              <a class="dropdown-item" href="./account/logout">Log out</a>
				            </div>
         				 </li>
					</c:when>
					<c:otherwise>
							<!-- <li class="nav-item active"> -->
							<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
							<li class="nav-item"><a class="nav-link" href="./account/register">Sign Up</a></li>
							<li class="nav-item"><a class="nav-link" href="./account/login">Log in</a></li>
							<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="./search">
				<input class="form-control mr-sm-2" type="text" placeholder="Search"
					aria-label="Search" name="keyword"> 
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
						<a class="nav-link" href="./searchindetail">Search in detail</a>
			
		</div>
	</nav>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<div align="center">
				<table>
				 	<tr>
		                <td colspan="2" align="center"><h2>Event Home</h2></td>
		          	</tr>
					<tr>
						<td>
							<table>
								<c:forEach items="${mediaEventPair.images}" var="image">  
						        	<tr>
						        		<td><img alt="${image.imageUrl}" src="${basePath}${image.imageUrl}" width="400" height="300"></td>
						        	</tr>
						    	</c:forEach>
					            <c:forEach items="${mediaEventPair.audios}" var="audio"> 
									<tr>
										<td><embed src="${basePath}${audio.audioUrl}" width="400" height="50" loop="false" autostart="false"/></td>
									</tr>
								</c:forEach> 
							</table>
						</td>
						<td>
							<table>
					            <tr>
					                <td>Event Name:</td>
					                <td>${mediaEventPair.event.eventName}</td>
					            </tr>
					            <tr>
					                <td>Event Organizer:</td>
					                <td>${mediaEventPair.event.organizer}</td>
					            </tr>
					            <tr>
					                <td>City:</td>
					                <td>${mediaEventPair.event.location}</td>
					            </tr>
					            <tr>
					                <td>Start Date:</td>
					                <td>${mediaEventPair.event.startDate}</td>
					            </tr>
					            <tr>
					                <td>End Date:</td>
					                <td>${mediaEventPair.event.endDate}</td>
					            </tr>
					            <tr>
					                <td>Capacity:</td>
					                <td>${mediaEventPair.event.capacity}</td>
					            </tr>
					            <tr>
					                <td>Fees ($AUD):</td>
					                <td>${mediaEventPair.event.fees}</td>
					            </tr>
					            <tr>
					                <td>Category:</td>
					                <td>${mediaEventPair.event.category}</td>
					            </tr>
					            <tr>
					                <td>
					                <form method="GET" action="./showlocationinmap">   
   									 <input type="hidden" name="latitude" value="${mediaEventPair.event.latitude}">
   									 <input type="hidden" name="longitude" value="${mediaEventPair.event.longitude}">
   									 <input type="submit" value="View location in Map">
   									 </form> 
   									   
					                </td>
					            </tr>
					        </table>
						</td>
					</tr>
					<tr>
		            	<td></td>
		            	<c:choose>
		            		<c:when test="${user.username == mediaEventPair.event.organizer}">
		      					<td align="right"><a href="./event/edit?eventId=${mediaEventPair.event.eventId}"><Button type="button">Edit</Button></a> <a href="./event/delete?eventId=${mediaEventPair.event.eventId}"><Button type="button">Delete</Button></a></td>
		      				</c:when>
		      				<c:otherwise>
		      					<td align="right"><a href="./booking/make?eventId=${mediaEventPair.event.eventId}"><Button type="button">Book</Button></a> <a href=""><Button type="button">Follow</Button></a></td>
		      				</c:otherwise>
		      			</c:choose>
					</tr>
				</table>
		    </div>
		    
		</div>    
    </div>
	<div class="container">
		<!-- Example row of columns -->

		<hr>

		<footer>
			<p>� Company 2017</p>
		</footer>
	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./static_resources/js/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="./static_resources/js/popper.min.js"></script>
	<script src="./static_resources/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./static_resources/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>